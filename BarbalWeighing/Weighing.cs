﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Net.Sockets;
using System.Threading;
using System.Net;
using System.Runtime.InteropServices;
using System.IO;
using System.Text.RegularExpressions;

namespace BarbalWeighing
{
    [ComVisible(true)]
    public class Weighing
    {

        public Weighing() { }


        /// <summary>
        /// Exist error
        /// </summary>
        public bool isError { get; set; }
        /// <summary>
        /// Message of error
        /// </summary>
        public string MessageError { get; set; }
        /// <summary>
        /// Value of reader weight
        /// </summary>
        public string dWeightReader { get; set; }
        /// <summary>
        /// Name of reader selected
        /// </summary>
        public string bReaderSelected { get; set; }
        /// <summary>
        /// Number of decimal places of weight result
        /// </summary>
        public int bDecimalPlaces { get; set; }
        /// <summary>
        /// Command for reading weight
        /// </summary>
        public string command_reader { get; set; }
        /// <summary>
        /// Command for make weighing reader to zero
        /// </summary>
        public string command_zero { get; set; }
        /// <summary>
        /// Command for rele
        /// </summary>
        public string command_rele { get; set; }

        /// <summary>
        /// Name of port COM
        /// </summary>
        public string PortName { get; set; }
        /// <summary>
        /// BaudRate of port COM
        /// </summary>
        public int BaudRate { get; set; }
        /// <summary>
        /// DataBits of port COM
        /// </summary>
        public int DataBits { get; set; }
        /// <summary>
        /// Parity of port COM
        /// </summary>
        public Parity Parity { get; set; }
        /// <summary>
        /// StopBits of port COM
        /// </summary>
        public StopBits StopBits { get; set; }

        public SerialPort serial;

        //private NetworkStream serverStream1;
        //private TcpClient clientSocket1;
        //private Thread ctThread1;

        /// <summary>
        /// IP address of reader
        /// </summary>
        public string IP { get; set; }
        /// <summary>
        /// Port TCP/IP of reader
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// Port COM or TCP/IP
        /// </summary>
        public bool NY_COM { get; set; }

        /// <summary>
        /// Reader Interval
        /// </summary>
        public int Reading_Interval { get; set; }

        #region COM PORT

        /// <summary>
        /// Init COM Port
        /// </summary>
        public void InitPortCOM()
        {
            if (serial?.IsOpen == true)
            {
                //serial.Close();
                serial.DiscardInBuffer();
                serial.Close();
                serial.Dispose();
            }

            try
            {
                serial = new SerialPort
                {
                    PortName = PortName,
                    BaudRate = BaudRate,
                    DataBits = DataBits,
                    Parity = Parity,
                    StopBits = StopBits
                };

                serial.Open();
            }
            catch (Exception ex)
            {
                isError = true;
                MessageError = ("Unable to init to comm port" + ex.Message);
                Console.WriteLine(MessageError);
                TerminateReaderCOM();
            }
        }

        /// <summary>
        /// Terminate COM Reader
        /// </summary>
        public void TerminateReaderCOM()
        {
            // Termina o protocolo para leitor
            //----------------------------------------------------------------
            try
            {
                if (serial == null)
                    return;

                if (serial.IsOpen)
                {
                    serial.Close();
                }

                serial.Dispose();
            }
            catch (Exception ex)
            {
                isError = true;
                MessageError = "Cannot terminate reader COM!" + ex.Message;
                Console.WriteLine(MessageError);
            }
        }

        /// <summary>
        /// Turn reader to zero value
        /// </summary>
        public void ResetReaderCOM()
        {
            try
            {
                InitPortCOM();
                if (!isError)
                {
                    Thread.Sleep(50);
                    // Verifica qual o leitor seleccionado
                    //------------------------------------------------------------
                    switch (bReaderSelected)
                    {
                        /* *08/07/2021*
                         * Adicionar opção "Repor zero da balança" para o leito W7
                         */
                        case "W7":
                            if (string.IsNullOrEmpty(command_zero))
                            {
                                command_zero = "SZ";
                            }
                            serial.Write(command_zero + Convert.ToChar(13).ToString());
                            break;
                        case "W9":
                            // Envia comando de zero ao leitor
                            if (string.IsNullOrEmpty(command_zero))
                            {
                                command_zero = "SC";
                            }
                            serial.Write(command_zero + Convert.ToChar(13).ToString());

                            break;
                        case "W10":
                            // Envia comando de zero ao leitor
                            if (string.IsNullOrEmpty(command_zero))
                            {
                                command_zero = "SC";
                            }
                            serial.Write(command_zero + Convert.ToChar(13).ToString());
                            break;
                        case "W11":
                            //serial.Write("SC" + Convert.ToChar(13).ToString());
                            serial.Write("");
                            break;
                        case "W12":
                            if (string.IsNullOrEmpty(command_zero))
                            {
                                command_zero = "Z";
                            }
                            serial.Write(command_zero + Convert.ToChar(13).ToString());
                            break;
                        case "W13":
                            // Envia comando de zero ao leitor
                            if (string.IsNullOrEmpty(command_zero))
                            {
                                command_zero = "Z";
                            }
                            serial.Write(command_zero + Convert.ToChar(13).ToString());
                            break;
                        case "W14":
                            // Envia comando de zero ao leitor
                            if (string.IsNullOrEmpty(command_zero))
                            {
                                command_zero = "Z";
                            }
                            serial.Write(command_zero + Convert.ToChar(13).ToString());
                            break;
                        case "W15":
                            // Envia comando de zero ao leitor
                            if (string.IsNullOrEmpty(command_zero))
                            {
                                command_zero = "Z";
                            }
                            serial.Write(command_zero + Convert.ToChar(13).ToString());
                            break;
                        case "W21":
                            // Envia comando de zero ao leitor
                            if (string.IsNullOrEmpty(command_zero))
                            {
                                command_zero = "SC";
                            }
                            serial.Write(command_zero + Convert.ToChar(13).ToString());
                            break;
                        case "W30":
                            // Envia comando de zero ao leitor
                            if (string.IsNullOrEmpty(command_zero))
                            {
                                command_zero = "ZERO";
                            }
                            serial.Write(command_zero + Convert.ToChar(13).ToString());
                            break;
                        case "ALFA 90":
                            if (string.IsNullOrEmpty(command_zero))
                            {
                                command_zero = "";
                            }
                            serial.Write(command_zero + Convert.ToChar(13).ToString());
                            break;
                        case "G1":
                            if (string.IsNullOrEmpty(command_zero))
                            {
                                command_zero = "CERO";
                            }
                            serial.Write(command_zero + Convert.ToChar(13).ToString());
                            break;
                        case "G2":
                            if (string.IsNullOrEmpty(command_zero))
                            {
                                command_zero = "CERO";
                            }
                            serial.Write(command_zero + Convert.ToChar(13).ToString());
                            break;
                        case "BIN-9001":
                            serial.Write("");
                            break;

                    }

                    isError = false;
                    MessageError = "";
                }
                else
                {
                    isError = true;
                    MessageError = "Unable to open comm port<br/>";
                    Console.WriteLine(MessageError);
                }
            }
            catch (Exception ex)
            {
                isError = true;
                MessageError = "Unable to write comm port<br/>" + ex.Message;
                Console.WriteLine(MessageError);
                TerminateReaderCOM();

            }
            finally
            {
                TerminateReaderCOM();
            }
        }

        public void TararReaderCOM(string valorTara)
        {
            try
            {
                InitPortCOM();
                if (!isError)
                {
                    Thread.Sleep(500);
                    // Verifica qual o leitor seleccionado
                    //------------------------------------------------------------
                    valorTara = valorTara.Replace(",", "").Replace(".", "");
                    while (valorTara.Length < 6)
                    {
                        valorTara = "0" + valorTara;
                    }

                    switch (bReaderSelected)
                    {
                        /* *08/07/2021*
                         * Adicionar opção "Tara manual" para o leito W7
                         */
                        case "W7":
                            serial.Write("TM" + valorTara + Convert.ToChar(13).ToString());
                            break;
                        case "W10":
                            //Envia comando tarar manual para o leitor
                            serial.Write("SJ" + valorTara + Convert.ToChar(13).ToString());
                            break;
                    }

                    isError = false;
                    MessageError = "";
                }
                else
                {
                    isError = true;
                    MessageError = "Unable to open comm port<br/>";
                    Console.WriteLine(MessageError);
                }
            }
            catch (Exception ex)
            {
                isError = true;
                MessageError = "Unable to write comm port<br/>" + ex.Message;
                TerminateReaderCOM();
            }
            finally
            {
                TerminateReaderCOM();
            }
        }

        public void DestararReaderCOM()
        {
            try
            {
                InitPortCOM();
                if (!isError)
                {
                    Thread.Sleep(50);
                    // Verifica qual o leitor seleccionado
                    //------------------------------------------------------------

                    switch (bReaderSelected)
                    {
                        /* *08/07/2021*
                         * Adicionar opção "Apagar Tara" para o leito W7
                         */
                        case "W7":
                            serial.Write("TC" + Convert.ToChar(13).ToString());
                            break;
                        case "W10":
                            // Envia comando destarar para o leitor
                            serial.Write("SD" + Convert.ToChar(13).ToString());
                            break;
                    }

                    isError = false;
                    MessageError = "";
                }
                else
                {
                    isError = true;
                    MessageError = "Unable to open comm port<br/>";
                    Console.WriteLine(MessageError);
                }
            }
            catch (Exception ex)
            {
                isError = true;
                MessageError = "Unable to write comm port<br/>" + ex.Message;
                Console.WriteLine(MessageError);
                TerminateReaderCOM();
            }
            finally
            {
                TerminateReaderCOM();
            }
        }

        /// <summary>
        /// Write DSD Memory
        /// </summary>
        public void WriteDSDReaderCOM()
        {
            try
            {
                InitPortCOM();
                if (!isError)
                {
                    Thread.Sleep(50);
                    // Verifica qual o leitor seleccionado
                    //------------------------------------------------------------
                    switch (bReaderSelected)
                    {
                        case "W9":
                            // Envia comando
                            serial.Write("SW" + Convert.ToChar(13).ToString());
                            break;
                        case "W10":
                            // Envia comando
                            serial.Write("SW" + Convert.ToChar(13).ToString());
                            break;

                    }

                    isError = false;
                    MessageError = "";
                }
                else
                {
                    isError = true;
                    MessageError = "Unable to open comm port<br/>";
                    Console.WriteLine(MessageError);
                }
            }
            catch (Exception ex)
            {
                isError = true;
                MessageError = "Unable to write comm port<br/>" + ex.Message;
                Console.WriteLine(MessageError);
                TerminateReaderCOM();
            }
            finally
            {
                TerminateReaderCOM();
            }
        }

        /// <summary>
        /// Read a weight written in reader or send a command to read
        /// </summary>
        public void ReadAndWriteCOM(string command)
        {
            isError = false;
            bool good_capture = false;
            string strCOMReception = "";

            InitPortCOM();

            if (isError)
            {
                isError = true;
                MessageError = "ReadAndWriteCOM<br/>";
                Console.WriteLine(MessageError);
                dWeightReader = null;
                return;
            }

            try
            {
                if (!string.IsNullOrEmpty(command))
                {
                    serial.Write(command);
                    Thread.Sleep(200);
                }

                while (!good_capture)
                {
                    String responseData = String.Empty;


                    Thread.Sleep(100);
                    int bytecount = serial.BytesToRead;

                    // Se nao recebeu dados sai
                    if (bytecount <= 0)
                    {
                        isError = true;
                        //MessageError = "O leitor não está a enviar dados!";
                        strCOMReception = null;
                        Console.WriteLine(MessageError);
                        break;
                    }
                    byte[] byteBuffer = new byte[bytecount + 1];

                    // Le dados no buffer de entrada
                    serial.Read(byteBuffer, 0, bytecount);
                    // Convert os dados recebido em string
                    responseData = System.Text.Encoding.ASCII.GetString(byteBuffer, 0, bytecount);


                    if (responseData != "")
                    {
                        Console.WriteLine(responseData);
                    }

                    /////////////////////////////////////////////////////////////////////////////////////////////////////

                    int posEnter = responseData.LastIndexOf(Convert.ToChar(13));
                    int posSignal = -1;

                    for (int i = posEnter; i >= 0; i--)
                    {
                        if (responseData[i].ToString() == "+" || responseData[i].ToString() == "-" || responseData[i].ToString() == Convert.ToChar(2).ToString())
                        {
                            posSignal = i;
                            i = 0;
                        }
                        else if (responseData[i].ToString() == "S" || responseData[i].ToString() == "T")
                        {
                            posSignal = i + 1;
                            i = 0;
                        }
                    }
                    if (bReaderSelected == "I200")
                    {
                        Regex r = new Regex(@"[0-9]+");
                        if (r.IsMatch(responseData))
                        {
                            string Cut0Left = r.Match(responseData).Value;
                            Cut0Left=Cut0Left.Remove(0,2);
                            int i = 0;
                            for (; i < Cut0Left.Length; i++)
                            {
                                if (Cut0Left[i] != '0')
                                {
                                    break;
                                }
                            }
                            int Cut0LeftLength = Cut0Left.Length;
                            Cut0Left =Cut0Left.Substring(i, Cut0LeftLength - i);
                            if (Cut0Left.Length==0)
                            {
                                Cut0Left = "0";
                            }
                            strCOMReception = Cut0Left;//r.Match(Cut0Left).Value;
                            good_capture = true;

                        }
                    }
                    else if (bReaderSelected == "L225")
                    {
                        Regex r2 = new Regex(@"[0-9\.]+");
                        if (r2.IsMatch(responseData))
                        {

                            strCOMReception = r2.Match(responseData).Value;
                            good_capture = true;
                        }

                    }
                    else if (bReaderSelected == "W70")
                    {
                        Regex r2 = new Regex(@"-?[0-9\.]+"); 
                        if (r2.IsMatch(responseData))
                        {

                            strCOMReception = r2.Match(responseData).Value;
                            //Console.WriteLine(posEnter);
                            //Console.WriteLine(posSignal);
                            good_capture = true;
                        }
                    }
                    /* *07/07/2021*
                     * Adicionar Leitor w7
                     */
                    else if (bReaderSelected == "W7")
                    {
                        // Formatar a string para obter apenas o peso
                        var responseDataSubstring = responseData.Substring(23, 8).Trim(' ');

                        Regex r2 = new Regex(@"-?[0-9\.]+");
                        if (r2.IsMatch(responseDataSubstring))
                        {
                            strCOMReception = r2.Match(responseDataSubstring).Value;
                            //Console.WriteLine(posEnter);
                            //Console.WriteLine(posSignal);
                            good_capture = true;
                        }
                    }
                    else if (posEnter != -1 && posSignal != -1)
                    {
                        strCOMReception = responseData.Substring(posSignal, posEnter - posSignal + 1);

                        string teste = "";
                        teste = FormatReceivedMensage(strCOMReception);
                        teste = teste.Replace("+", "").Replace("-", "").Replace(".", ",");
                        try
                        {
                            decimal.Parse(teste);
                            good_capture = true;
                            isError = false;
                            MessageError = string.Empty;
                        }
                        catch { good_capture = false; }
                    }
                }
            }
            catch (Exception ex)
            {
                isError = true;
                MessageError = "ReadAndWriteCOM<br/>" + ex.Message;
                Console.WriteLine(MessageError);
                //TerminateReaderCOM();
            }

            TerminateReaderCOM();

            if (!string.IsNullOrEmpty(strCOMReception))
            {
                strCOMReception = FormatReceivedMensage(strCOMReception);
                dWeightReader = strCOMReception.Replace(",", ".");
                strCOMReception = "";
            }
            else
            {
                strCOMReception = "";
                dWeightReader = null;
                isError = true;
                MessageError = ("Falha de comunicação com o leitor!");
                Console.WriteLine(MessageError);
            }
        }

        #endregion

        #region TCP/IP

        /// <summary>
        /// Turn reader to zero value
        /// </summary>
        public void ResetReaderTCPIP()
        {
            byte[] outStream1 = Encoding.ASCII.GetBytes(Convert.ToChar(13).ToString());
            TcpClient client = new TcpClient();

            try
            {

                client = Connect(IP, Port, 2);

                if (client.Connected)
                {
                    // Verifica qual o leitor seleccionado
                    //------------------------------------------------------------
                    switch (bReaderSelected)
                    {
                        /* *08/07/2021*
                         * Adicionar opção "Repor zero da balança" para o leito W7
                         */
                        case "W7":
                            if (string.IsNullOrEmpty(command_zero))
                            {
                                command_zero = "SZ";
                            }
                            serial.Write(command_zero + Convert.ToChar(13).ToString());
                            break;
                        case "W9":
                            // Envia comando de zero ao leitor
                            if (string.IsNullOrEmpty(command_zero))
                            {
                                command_zero = "SC";
                            }
                            outStream1 = Encoding.ASCII.GetBytes(command_zero + Convert.ToChar(13).ToString());
                            break;
                        case "W10":
                            // Envia comando de zero ao leitor
                            if (string.IsNullOrEmpty(command_zero))
                            {
                                command_zero = "SC";
                            }
                            outStream1 = Encoding.ASCII.GetBytes(command_zero + Convert.ToChar(13).ToString());
                            break;
                        case "W11":
                            if (string.IsNullOrEmpty(command_zero))
                            {
                                command_zero = "";
                            }
                            outStream1 = Encoding.ASCII.GetBytes(command_zero + Convert.ToChar(13).ToString());
                            break;
                        case "W12":
                            if (string.IsNullOrEmpty(command_zero))
                            {
                                command_zero = "Z";
                            }
                            outStream1 = Encoding.ASCII.GetBytes(command_zero + Convert.ToChar(13).ToString());
                            break;
                        case "W13":
                            if (string.IsNullOrEmpty(command_zero))
                            {
                                command_zero = "Z";
                            }
                            outStream1 = Encoding.ASCII.GetBytes(command_zero + Convert.ToChar(13).ToString());
                            break;
                        case "W14":
                            // Envia comando de zero ao leitor
                            if (string.IsNullOrEmpty(command_zero))
                            {
                                command_zero = "Z";
                            }
                            outStream1 = Encoding.ASCII.GetBytes(command_zero + Convert.ToChar(13).ToString());
                            break;
                        case "W15":
                            // Envia comando de zero ao leitor
                            if (string.IsNullOrEmpty(command_zero))
                            {
                                command_zero = "Z";
                            }
                            outStream1 = Encoding.ASCII.GetBytes(command_zero + Convert.ToChar(13).ToString());
                            break;
                        case "W21":
                            if (string.IsNullOrEmpty(command_zero))
                            {
                                command_zero = "SC";
                            }
                            outStream1 = Encoding.ASCII.GetBytes(command_zero + Convert.ToChar(13).ToString());
                            break;
                        case "W30":
                            if (string.IsNullOrEmpty(command_zero))
                            {
                                command_zero = "ZERO";
                            }
                            outStream1 = Encoding.ASCII.GetBytes(command_zero + Convert.ToChar(13).ToString());
                            break;
                        case "ALFA 90":
                            if (string.IsNullOrEmpty(command_zero))
                            {
                                command_zero = "";
                            }
                            outStream1 = Encoding.ASCII.GetBytes(command_zero + Convert.ToChar(13).ToString());
                            break;
                        case "G1":
                            if (string.IsNullOrEmpty(command_zero))
                            {
                                command_zero = "CERO";
                            }
                            outStream1 = Encoding.ASCII.GetBytes(command_zero + Convert.ToChar(13).ToString());
                            break;
                        case "G2":
                            if (string.IsNullOrEmpty(command_zero))
                            {
                                command_zero = "CERO";
                            }
                            outStream1 = Encoding.ASCII.GetBytes(command_zero + Convert.ToChar(13).ToString());
                            break;
                        case "BIN-9001":
                            if (string.IsNullOrEmpty(command_zero))
                            {
                                command_zero = "";
                            }
                            outStream1 = Encoding.ASCII.GetBytes(command_zero + Convert.ToChar(13).ToString());
                            break;
                    }
                    Thread.Sleep(50);

                    // Get a client stream for reading and writing.
                    NetworkStream stream = client.GetStream();
                    stream.Write(outStream1, 0, outStream1.Length);
                    stream.Flush();

                    isError = false;
                    MessageError = "";
                    stream.Close();
                    stream.Dispose();
                }
                else
                {
                    isError = true;
                    MessageError = ("Falha ao comunicar com " + IP + ":" + Port + "!");
                    Console.WriteLine(MessageError);
                }
            }
            catch (Exception ex)
            {
                isError = true;
                MessageError = ex.Message;
                Console.WriteLine(MessageError);
            }
            finally
            {
                client.Close();
            }
        }

        /// <summary>
        /// Write DSD Memory
        /// </summary>
        public void WriteDSDReaderTCPIP()
        {
            byte[] outStream1 = Encoding.ASCII.GetBytes(Convert.ToChar(13).ToString());
            TcpClient client = new TcpClient();

            try
            {
                client = Connect(IP, Port, 2);

                if (client.Connected)
                {
                    // Verifica qual o leitor seleccionado
                    //------------------------------------------------------------
                    switch (bReaderSelected)
                    {
                        case "W9":
                            // Envia comando
                            outStream1 = Encoding.ASCII.GetBytes("SW" + Convert.ToChar(13).ToString());
                            break;
                        case "W10":
                            // Envia comando
                            outStream1 = Encoding.ASCII.GetBytes("SW" + Convert.ToChar(13).ToString());
                            break;

                    }
                    Thread.Sleep(50);
                    // Get a client stream for reading and writing.
                    NetworkStream stream = client.GetStream();
                    stream.Write(outStream1, 0, outStream1.Length);
                    stream.Flush();

                    isError = false;
                    MessageError = "";
                }
                else
                {
                    isError = true;
                    MessageError = ("Falha ao comunicar com " + IP + ":" + Port + "!");
                    Console.WriteLine(MessageError);
                    Thread.Sleep(50);

                }
            }
            catch (Exception ex)
            {
                isError = true;
                MessageError = ex.Message;
                Console.WriteLine(MessageError);

            }
            finally
            {
                client.Close();
            }
        }

        private TcpClient Connect(string hostName, int port, int timeout)
        {
            TcpClient client = new TcpClient();

            //when the connection completes before the timeout it will cause a race
            //we want EndConnect to always treat the connection as successful if it wins

            IAsyncResult ar = client.BeginConnect(hostName, port, null, null);
            ar.AsyncWaitHandle.WaitOne(TimeSpan.FromSeconds(timeout));

            return client;
        }

        /// <summary>
        /// Read a weight written in reader or send a command to read
        /// </summary>
        /// <param name="command"></param>

        public void ReadAndWriteTCPIP(string command)
        {
            new Thread(() =>
            {
                //Thread.CurrentThread.IsBackground = true;


                isError = false;
                bool good_capture = false;
                string strCOMReception = "";
                TcpClient client = new TcpClient();

                try
                {
                    Thread.Sleep(200);
                    // Create a TcpClient.
                    client = Connect(IP, Port, 20);


                    if (!client.Connected)
                    {
                        dWeightReader = null;
                        client.Close();
                        isError = true;
                        MessageError = ("Falha ao comunicar com " + IP + ":" + Port + "!");


                        Console.WriteLine(MessageError);


                        return;

                    }

                    // Get a client stream for reading and writing.
                    NetworkStream stream = client.GetStream();

                    // Buffer to store the response bytes.
                    Byte[] data = data = new Byte[256];

                    if (!string.IsNullOrEmpty(command))
                    {
                        data = Encoding.ASCII.GetBytes(command);
                        // Send the message to the connected TcpServer. 
                        stream.Write(data, 0, data.Length);
                        stream.Flush();
                        Thread.Sleep(300);
                    }



                    while (!good_capture)
                    {
                        // String to store the response ASCII representation.
                        String responseData = String.Empty;

                        data = new Byte[256];
                        // Read the first batch of the TcpServer response bytes.

                        //Int32 bytes = stream.Read(data, 0, data.Length);
                        Int32 bytes = 0;
                        Thread.Sleep(500);
                        //verifica se a stream contem dados
                        if (stream.DataAvailable)
                        {
                            bytes = stream.Read(data, 0, data.Length);
                        }

                        // Se nao recebeu dados sai
                        if (bytes <= 0)
                        {
                            isError = true;
                            MessageError = "Leitor não está a enviar dados!";
                            WriteToLog(MessageError);
                            strCOMReception = null;

                            Console.WriteLine(MessageError);
                            break;

                        }

                        responseData = Encoding.ASCII.GetString(data, 0, bytes);

                        int posEnter = responseData.LastIndexOf(Convert.ToChar(13));
                        int posSignal = -1;


                        if (bReaderSelected == "L225" || bReaderSelected == "I200")
                        {
                            Console.WriteLine("-------------");
                            Console.WriteLine("First");
                            Console.WriteLine(responseData);

                            //Regex r = new Regex(@"[0-9.]+");
                            Regex r = new Regex(@"(.+)[0-9.]+(.+)[\r\n]");
                            //Regex r = new Regex(@"[0-9.]+(.+)[\r\n]");
                            if (r.IsMatch(responseData))
                            {
                                Regex rearR = new Regex(@"[0-9.]+");
                                Console.WriteLine("Second");
                                Console.WriteLine(rearR.Match(responseData).Value);

                                
                                strCOMReception = rearR.Match(responseData).Value;
                                string teste = strCOMReception.Replace(Convert.ToChar(13).ToString(), "");
                                teste = FormatReceivedMensage(strCOMReception);
                                teste = teste.Replace("+", "").Replace("-", "").Replace(".", ",");
                                Console.WriteLine("Third");
                                Console.WriteLine(teste);
                                try
                                {
                                    decimal.Parse(teste);
                                    good_capture = true;
                                }
                                catch {
                                    Console.WriteLine("Error Catch <--------");
                                    good_capture = false; }
                            }
                        }
                        else if (bReaderSelected == "W70")
                        {
                            Regex r2 = new Regex(@"-?[0-9\.]+");
                            if (r2.IsMatch(responseData))
                            {
                                strCOMReception = r2.Match(responseData).Value;
                                //Console.WriteLine(posEnter);
                                //Console.WriteLine(posSignal);
                                good_capture = true;
                            }
                        }
                        /* *07/07/2021*
                         * Adicionar Leitor w7
                         */
                        else if (bReaderSelected == "W7")
                        {
                            // Formatar a string para obter apenas o peso
                            var responseDataSubstring = responseData.Substring(23,8).Trim(' ');

                            Regex r2 = new Regex(@"-?[0-9\.]+");

                            if (r2.IsMatch(responseDataSubstring))
                            {
                                strCOMReception = r2.Match(responseDataSubstring).Value;
                                //Console.WriteLine(posEnter);
                                //Console.WriteLine(posSignal);
                                good_capture = true;
                            }
                        }
                        else
                        {
                            for (int i = posEnter; i >= 0; i--)
                            {
                                if (responseData[i].ToString() == "+" || responseData[i].ToString() == "-" || responseData[i].ToString() == Convert.ToChar(2).ToString())
                                {
                                    posSignal = i;
                                    i = 0;
                                }
                                else if (responseData[i].ToString() == "S" || responseData[i].ToString() == "T")
                                {
                                    posSignal = i + 1;
                                    i = 0;
                                }
                            }

                            if (posEnter != -1 && posSignal != -1)
                            {
                                strCOMReception = responseData.Substring(posSignal, posEnter - posSignal + 1);

                                string teste = strCOMReception.Replace(Convert.ToChar(13).ToString(), "");
                                teste = FormatReceivedMensage(strCOMReception);
                                teste = teste.Replace("+", "").Replace("-", "").Replace(".", ",");

                                try
                                {
                                    decimal.Parse(teste);
                                    good_capture = true;
                                }
                                catch { good_capture = false; }
                            }

                        }

                    }

                    stream.Close();
                    stream.Dispose();
                    client.Close();

                    if (!string.IsNullOrEmpty(strCOMReception))
                    {
                        strCOMReception = FormatReceivedMensage(strCOMReception);

                        dWeightReader = strCOMReception.Replace(", ", ".");
                        //Console.WriteLine("dWeightReader "+ dWeightReader);
                        Console.WriteLine(dWeightReader);
                        strCOMReception = "";

                        isError = false;
                        MessageError = string.Empty;
                        return;
                    }
                    else
                    {
                        dWeightReader = null;
                        strCOMReception = "";
                        isError = true;
                        MessageError = "Falha de comunicação com o leitor!";

                        Console.WriteLine(MessageError);
                    }
                }
                catch (Exception ex)
                {
                    dWeightReader = null;
                    isError = true;
                    MessageError = ex.Message;


                    Console.WriteLine(MessageError);
                }



            }).Start();
            return;

        }


        //public void ReadAndWriteTCPIP(string command)
        //{
        //    isError = false;
        //    bool good_capture = false;
        //    string strCOMReception = "";
        //    TcpClient client = new TcpClient();

        //    try
        //    {
        //        // Create a TcpClient.
        //        client = Connect(IP, Port, 20);


        //        if (!client.Connected)
        //        {
        //            dWeightReader = null;
        //            client.Close();
        //            isError = true;
        //            MessageError = ("Falha ao comunicar com " + IP + ":" + Port + "!");


        //            Console.WriteLine(MessageError);


        //            return;

        //        }

        //        // Get a client stream for reading and writing.
        //        NetworkStream stream = client.GetStream();

        //        // Buffer to store the response bytes.
        //        Byte[] data = data = new Byte[256];

        //        if (!string.IsNullOrEmpty(command))
        //        {
        //            data = Encoding.ASCII.GetBytes(command);
        //            // Send the message to the connected TcpServer. 
        //            stream.Write(data, 0, data.Length);
        //            stream.Flush();

        //            Thread.Sleep(300);
        //        }



        //        while (!good_capture)
        //        {
        //            // String to store the response ASCII representation.
        //            String responseData = String.Empty;

        //            data = new Byte[256];
        //            // Read the first batch of the TcpServer response bytes.

        //            //Int32 bytes = stream.Read(data, 0, data.Length);
        //            Int32 bytes = 0;
        //            Thread.Sleep(500);
        //            //verifica se a stream contem dados
        //            if (stream.DataAvailable)
        //            {
        //                bytes = stream.Read(data, 0, data.Length);
        //            }

        //            // Se nao recebeu dados sai
        //            if (bytes <= 0)
        //            {
        //                isError = true;
        //                MessageError = "Leitor não está a enviar dados!";
        //                strCOMReception = null;

        //                Console.WriteLine(MessageError);

        //                break;
        //            }

        //            responseData = Encoding.ASCII.GetString(data, 0, bytes);

        //            int posEnter = responseData.LastIndexOf(Convert.ToChar(13));
        //            int posSignal = -1;

        //            for (int i = posEnter; i >= 0; i--)
        //            {
        //                if (responseData[i].ToString() == "+" || responseData[i].ToString() == "-" || responseData[i].ToString() == Convert.ToChar(2).ToString())
        //                {
        //                    posSignal = i;
        //                    i = 0;
        //                }
        //                else if (responseData[i].ToString() == "S" || responseData[i].ToString() == "T")
        //                {
        //                    posSignal = i + 1;
        //                    i = 0;
        //                }
        //            }

        //            if (posEnter != -1 && posSignal != -1)
        //            {
        //                strCOMReception = responseData.Substring(posSignal, posEnter - posSignal + 1);

        //                string teste = strCOMReception.Replace(Convert.ToChar(13).ToString(), "");
        //                teste = FormatReceivedMensage(strCOMReception);
        //                teste = teste.Replace("+", "").Replace("-", "").Replace(".", ",");

        //                try
        //                {
        //                    decimal.Parse(teste);
        //                    good_capture = true;
        //                }
        //                catch { good_capture = false; }
        //            }
        //        }

        //        stream.Close();
        //        stream.Dispose();
        //        client.Close();

        //        if (!string.IsNullOrEmpty(strCOMReception))
        //        {
        //            strCOMReception = FormatReceivedMensage(strCOMReception);

        //            dWeightReader = strCOMReception.Replace(",", ".");

        //            strCOMReception = "";

        //            isError = false;
        //            MessageError = string.Empty;
        //        }
        //        else
        //        {
        //            dWeightReader = null;
        //            strCOMReception = "";
        //            isError = true;
        //            MessageError = "Falha de comunicação com o leitor!";

        //            Console.WriteLine(MessageError);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        dWeightReader = null;
        //        isError = true;
        //        MessageError = ex.Message;


        //        Console.WriteLine(MessageError);
        //    }
        //}
        #endregion

        #region TOOLS

        /// <summary>
        /// Format a string returned by reader
        /// </summary>
        private string FormatReceivedMensage(string message)
        {
            string strCOMReception = message;

            if (strCOMReception.Length > 7)
            {
                // filtros da mensagem recebida
                //------------------------------------------------------------
                if (bReaderSelected == "W15" || bReaderSelected == "W14")
                {
                    strCOMReception = strCOMReception.Substring(strCOMReception.Length - 11, 11);
                }

                strCOMReception = strCOMReception.Replace(" ", "");
                strCOMReception = strCOMReception.Replace(Convert.ToChar(0).ToString(), "");
                strCOMReception = strCOMReception.Replace(Convert.ToChar(1).ToString(), "");
                strCOMReception = strCOMReception.Replace(Convert.ToChar(2).ToString(), "");
                strCOMReception = strCOMReception.Replace(Convert.ToChar(3).ToString(), "");
                strCOMReception = strCOMReception.Replace(Convert.ToChar(4).ToString(), "");
                strCOMReception = strCOMReception.Replace(Convert.ToChar(5).ToString(), "");
                strCOMReception = strCOMReception.Replace(Convert.ToChar(6).ToString(), "");
                strCOMReception = strCOMReception.Replace(Convert.ToChar(7).ToString(), "");
                strCOMReception = strCOMReception.Replace(Convert.ToChar(8).ToString(), "");
                strCOMReception = strCOMReception.Replace(Convert.ToChar(9).ToString(), "");
                strCOMReception = strCOMReception.Replace(Convert.ToChar(10).ToString(), "");
                strCOMReception = strCOMReception.Replace(Convert.ToChar(13).ToString(), "");
                strCOMReception = strCOMReception.Replace(Convert.ToChar(33).ToString(), "");
                strCOMReception = strCOMReception.Replace(Convert.ToChar(71).ToString(), "");
                strCOMReception = strCOMReception.Replace(Convert.ToChar(75).ToString(), "");
                strCOMReception = strCOMReception.Replace(Convert.ToChar(77).ToString(), "");
                strCOMReception = strCOMReception.Replace(Convert.ToChar(65).ToString(), "");
                strCOMReception = strCOMReception.Replace("+", "");
                strCOMReception = strCOMReception.Replace(".", ",");

                strCOMReception = Decimal.Parse(strCOMReception).ToString();
                strCOMReception = Math.Round(decimal.Parse(strCOMReception.ToString()), bDecimalPlaces).ToString();//Arredonda(strCOMReception, bDecimalPlaces);

            }

            return strCOMReception;
        }

        /// <summary>
        /// Send a request for return weight
        /// </summary>
        /// <param name="op">COM/TCP</param>
        /// <param name="write">True/False if write a command</param>
        public void SendRequestReaderWeight(string op, bool write)
        {
            if (write)
            {
                StringBuilder command = new StringBuilder();
                // Verifica qual o leitor seleccionado
                //------------------------------------------------------------
                switch (bReaderSelected)
                {
                    /* *08/07/2021*
                       * Adicionar opção "Ler peso (sem movimento)" para o leito W7
                       */
                    case "W7":
                        if (string.IsNullOrEmpty(command_reader))
                        {
                            command_reader = "RM";
                        }

                        command.Append(command_reader);
                        command.Append(Convert.ToChar(13).ToString());
                        break;

                    case "W9":
                        // Comando de pedido peso Leitor
                        if (string.IsNullOrEmpty(command_reader))
                        {
                            command_reader = "SB";
                        }

                        command.Append(command_reader);
                        command.Append(Convert.ToChar(13).ToString());
                        break;
                    case "W10":
                        // Comando de pedido peso Leitor
                        if (string.IsNullOrEmpty(command_reader))
                        {
                            command_reader = "SB";
                        }
                        command.Append(command_reader);
                        command.Append(Convert.ToChar(13).ToString());
                        break;
                    case "W11":
                        // Comando de pedido peso Leitor
                        if (string.IsNullOrEmpty(command_reader))
                        {
                            command_reader = "SN";
                        }
                        command.Append(command_reader);
                        command.Append(Convert.ToChar(13).ToString());
                        break;
                    case "W12":
                        // Comando de pedido peso Leitor
                        if (string.IsNullOrEmpty(command_reader))
                        {
                            command_reader = "P";
                        }
                        command.Append(command_reader);
                        command.Append(Convert.ToChar(13).ToString());
                        break;
                    case "W13":
                        // Comando de pedido peso Leitor
                        if (string.IsNullOrEmpty(command_reader))
                        {
                            command_reader = "P";
                        }
                        command.Append(command_reader);
                        command.Append(Convert.ToChar(13).ToString());
                        break;
                    case "W14":
                        // Comando de pedido peso Leitor
                        if (string.IsNullOrEmpty(command_reader))
                        {
                            command_reader = "S";
                        }
                        command.Append(command_reader);
                        command.Append(Convert.ToChar(13).ToString());
                        break;
                    case "W15":
                        // Comando de pedido peso Leitor
                        if (string.IsNullOrEmpty(command_reader))
                        {
                            command_reader = "R";
                        }
                        command.Append(command_reader);
                        command.Append(Convert.ToChar(13).ToString());
                        break;
                    case "W21":
                        // Comando de pedido peso Leitor
                        if (string.IsNullOrEmpty(command_reader))
                        {
                            command_reader = "SB";
                        }
                        command.Append(command_reader);
                        command.Append(Convert.ToChar(13).ToString());
                        break;
                    case "W30":
                        // Comando de pedido peso Leitor
                        if (string.IsNullOrEmpty(command_reader))
                        {
                            command_reader = "NETR";
                        }
                        command.Append(command_reader);
                        command.Append(Convert.ToChar(13).ToString());
                        break;
                    case "IBP":
                        // Comando de pedido peso Leitor
                        if (string.IsNullOrEmpty(command_reader))
                        {
                            command_reader = Convert.ToChar(2).ToString() + Convert.ToChar(86).ToString() + Convert.ToChar(3).ToString();
                        }
                        else
                        {
                            command_reader = Convert.ToChar(2).ToString() + command_reader + Convert.ToChar(3).ToString();
                        }
                        command.Append(command_reader);
                        break;
                    case "ALFA 90":
                        if (string.IsNullOrEmpty(command_reader))
                        {
                            command_reader = Convert.ToChar(6).ToString() + Convert.ToChar(5).ToString();
                        }
                        command.Append(command_reader);
                        command.Append(Convert.ToChar(13).ToString());
                        break;
                    case "G1":
                        // Comando de pedido peso Leitor
                        if (string.IsNullOrEmpty(command_reader))
                        {
                            command_reader = "NETO";
                        }
                        command.Append(command_reader);
                        command.Append(Convert.ToChar(13).ToString());
                        break;
                    case "G2":
                        // Comando de pedido peso Leitor
                        if (string.IsNullOrEmpty(command_reader))
                        {
                            command_reader = "LIQUIDO";
                        }
                        command.Append(command_reader);
                        command.Append(Convert.ToChar(13).ToString());
                        break;
                    case "BIN-9001":
                        // Comando de pedido peso Leitor
                        if (string.IsNullOrEmpty(command_reader))
                        {
                            command_reader = "$";
                        }
                        command.Append(command_reader);
                        command.Append(Convert.ToChar(13).ToString());
                        break;
                    case "L225":
                        command.Append(Convert.ToChar(05).ToString());
                        command.Append(Convert.ToChar(13).ToString());
                        /* byte[] bytestosend = { 0x05 };
                         serial.Write(bytestosend, 0, bytestosend.Length);*/
                        break;
                    case "I200":
                        command.Append(Convert.ToChar(0x01).ToString());
                        command.Append(Convert.ToChar(0x05).ToString());
                        command.Append(Convert.ToChar(0x30).ToString());
                        command.Append(Convert.ToChar(0x31).ToString());
                        command.Append(Convert.ToChar(0x4C).ToString());
                        command.Append(Convert.ToChar(0x0D).ToString());
                        command.Append(Convert.ToChar(0x0A).ToString());
                        /* byte[] bytestosend = { 0x05 };
                         serial.Write(bytestosend, 0, bytestosend.Length);*/
                        break;
                }

                if (op == "COM")
                {
                    ReadAndWriteCOM(command.ToString());
                }
                else if (op == "TCP")
                {
                    ReadAndWriteTCPIP(command.ToString());
                }
            }
            else
            {
                if (op == "COM")
                {
                    ReadAndWriteCOM(null);
                }
                else if (op == "TCP")
                {
                    ReadAndWriteTCPIP(null);
                }
            }
        }

        public void SendCustomRequest(Byte[] data)
        {
            InitPortCOM();
            serial.Write(data,0,data.Length);
            TerminateReaderCOM();
        }

        public static string Arredonda(decimal number, int decimalplaces)
        {
            string num = number.ToString();
            num = Arredonda(num, decimalplaces);

            return num;
        }
        public static string Arredonda(string num, int decimalplaces)
        {
            string[] aux = null;

            if (num.Contains(","))
            {
                aux = num.Split(',');
            }
            else if (num.Contains("."))
            {
                aux = num.Split('.');
            }
            else
            {
                if (decimalplaces != 0)
                {
                    num = num + ",";
                    for (int i = 0; i < decimalplaces; i++)
                    {
                        num += "0";
                    }
                }

                return num;
            }

            if (aux[0] == string.Empty)
                aux[0] = "0";

            if (decimalplaces != 0)
            {
                if (aux[1] == string.Empty)
                {
                    num = aux[0] + ",";
                    for (int i = 0; i < decimalplaces; i++)
                    {
                        num += "0";
                    }

                }
                else if (aux[1].Length == 1)
                {
                    num = aux[0] + "," + aux[1];

                    for (int i = 1; i < decimalplaces; i++)
                    {
                        num += "0";
                    }
                }
                else if (aux[1].Length == 2)
                {
                    num = aux[0] + "," + aux[1];

                    for (int i = 2; i < decimalplaces; i++)
                    {
                        num += "0";
                    }
                }
                else if (aux[1].Length > 2)
                {
                    num = Math.Round(Decimal.Parse(num), decimalplaces).ToString();
                }
            }
            else
            {
                num = Math.Round(Decimal.Parse(num), 0).ToString();
            }


            return num;
        }

        #endregion





        private void WriteToLog(string messageError)
        {
            try
            {
                //string fileName = $"WeighingLog{DateTime.Now.ToShortDateString().Replace("/", "-")}.txt";
                //string filePath = new Uri($"{Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory)}\\Logs\\{fileName}").LocalPath;
                string fileName = $"TestLog";
                string filePath = @"C:\Projetos\BarbalWeighing\BarbalWeighing\";
                if (!File.Exists(filePath))
                {
                    File.Create(filePath).Dispose();
                }
                using (StreamWriter sw = File.AppendText(filePath))
                {
                    sw.WriteLine("//********ERROR*********//");
                    sw.WriteLine($"{messageError} --> {DateTime.Now}");
                    sw.WriteLine("//********ERROR*********//");
                    sw.WriteLine(" ");
                }
            }
            catch (Exception)
            {
            }
        }


    }

}
