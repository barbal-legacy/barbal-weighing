﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BarbalWeighing;

namespace BarbalWeighingDiagnostic
{
    class Program
    {
        static void Main(string[] args)
        {
            // Comandos para execução do leitor w7 usando porta tcp/ip
            Weighing weighing = new Weighing();

            weighing.NY_COM = false;
            weighing.bReaderSelected = "W7";
            weighing.bDecimalPlaces = 2;
            //weighing.IP = "192.168.91.43";
            //weighing.Port = 4001;
            weighing.BaudRate = 9600;
            weighing.DataBits = 8;
            weighing.StopBits = System.IO.Ports.StopBits.One;
            weighing.Parity = System.IO.Ports.Parity.None;
            weighing.PortName = "COM3";


            weighing.SendRequestReaderWeight("COM", true);

            Console.WriteLine(weighing.dWeightReader);

            Console.ReadLine();
            //----------------------------------------------------------------------------//
        }
    }
}
